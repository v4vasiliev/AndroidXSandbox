package ru.vasiliev.sandbox.camera.device.camera.camera2;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.view.Surface;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.vasiliev.sandbox.camera.device.camera.common.CameraPreview;
import ru.vasiliev.sandbox.camera.device.camera.util.CameraFacing;
import ru.vasiliev.sandbox.camera.device.camera.util.CameraFlash;

class Camera2RequestManager {

    private Camera2Controller camera2Controller;
    private Camera2Config camera2Config;
    private CameraPreview cameraPreview;

    Camera2RequestManager(@NonNull Camera2Controller camera2Controller, @NonNull Camera2Config camera2Config,
                          @NonNull CameraPreview cameraPreview) {
        this.camera2Controller = camera2Controller;
        this.camera2Config = camera2Config;
        this.cameraPreview = cameraPreview;
    }

    private void setup3AControls(CaptureRequest.Builder requestBuilder, boolean autoFocus, CameraFlash cameraFlash,
                                 boolean autoWhiteBalance) {
        // Enable auto-magical 3A run
        requestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);

        // Auto focus
        if (autoFocus && camera2Config.isAfSupported()) {
            requestBuilder.set(CaptureRequest.CONTROL_AF_MODE, camera2Config.getOptimalAfMode());
        } else {
            requestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
        }

        // Flash and auto exposure
        requestBuilder.set(CaptureRequest.CONTROL_AE_MODE, cameraFlash.getAeMode());
        requestBuilder.set(CaptureRequest.FLASH_MODE, cameraFlash.getFlashMode());

        // Auto white balance
        if (autoWhiteBalance && camera2Config.isAwbSupported()) {
            requestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO);
        } else {
            requestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_OFF);
        }
    }

    private void setupJpegOrientation(CaptureRequest.Builder requestBuilder, CameraFacing cameraFacing,
                                      int displayOrientation) {
        requestBuilder.set(CaptureRequest.JPEG_ORIENTATION,
                           (camera2Config.getCameraSensorOrientation() +
                            displayOrientation * (cameraFacing == CameraFacing.FRONT ? 1 : -1) + 360) % 360);
    }

    /**
     * @return Preview request builder with actual preview params from linked controller
     * - Auto-focus
     * - Flash
     * - Auto white-balance
     * @throws CameraAccessException when camera is not accessible
     */
    PreviewRequestBuilder newPreviewRequestBuilder() throws CameraAccessException {
        return new PreviewRequestBuilder();
    }

    /**
     * @return Capture request builder with actual preview params from linked controller:
     * - Auto-focus
     * - Flash
     * - Auto white-balance
     * - Camera facing
     * - Display rotation
     * @throws CameraAccessException when camera is not accessible
     */
    CaptureRequestBuilder newCaptureRequestBuilder() throws CameraAccessException {
        return new CaptureRequestBuilder();
    }

    public class PreviewRequestBuilder {

        private CaptureRequest.Builder previewRequestBuilder;
        private List<Surface> outputSurfaces = new ArrayList<>();
        private boolean autoFocus = camera2Controller.getAutoFocus();
        private CameraFlash cameraFlash = camera2Controller.getFlash();
        private boolean autoWhiteBalance = camera2Controller.getAutoWhiteBalance();

        private PreviewRequestBuilder() throws CameraAccessException {
            previewRequestBuilder = camera2Controller.getCameraDevice().createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        }

        PreviewRequestBuilder setOutputSurface(Surface outputSurface) {
            outputSurfaces.add(outputSurface);
            return this;
        }

        PreviewRequestBuilder setOutputSurfaces(List<Surface> outputSurfaces) {
            this.outputSurfaces = outputSurfaces;
            return this;
        }

        PreviewRequestBuilder setAutoFocus(boolean autoFocus) {
            this.autoFocus = autoFocus;
            return this;
        }

        PreviewRequestBuilder setCameraFlash(CameraFlash cameraFlash) {
            this.cameraFlash = cameraFlash;
            return this;
        }

        PreviewRequestBuilder setAutoWhiteBalance(boolean autoWhiteBalance) {
            this.autoWhiteBalance = autoWhiteBalance;
            return this;
        }

        <T> PreviewRequestBuilder setKeyValue(@NonNull CaptureRequest.Key<T> key, T value) {
            previewRequestBuilder.set(key, value);
            return this;
        }

        public CaptureRequest build() {
            setup3AControls(previewRequestBuilder, autoFocus, cameraFlash, autoWhiteBalance);
            for (Surface surface : outputSurfaces) {
                previewRequestBuilder.addTarget(surface);
            }
            return previewRequestBuilder.build();
        }
    }

    public class CaptureRequestBuilder {

        private CaptureRequest.Builder previewRequestBuilder;
        private List<Surface> outputSurfaces = new ArrayList<>();
        private boolean autoFocus = camera2Controller.getAutoFocus();
        private CameraFlash flash = camera2Controller.getFlash();
        private boolean autoWhiteBalance = camera2Controller.getAutoWhiteBalance();
        private CameraFacing cameraFacing = camera2Controller.getFacing();
        private int displayOrientation = cameraPreview.getDisplayOrientation();

        private CaptureRequestBuilder() throws CameraAccessException {
            previewRequestBuilder = camera2Controller.getCameraDevice().createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
        }

        CaptureRequestBuilder setOutputSurface(Surface outputSurface) {
            outputSurfaces.add(outputSurface);
            return this;
        }

        CaptureRequestBuilder setOutputSurfaces(List<Surface> outputSurfaces) {
            this.outputSurfaces = outputSurfaces;
            return this;
        }

        CaptureRequestBuilder setAutoFocus(boolean autoFocus) {
            this.autoFocus = autoFocus;
            return this;
        }

        CaptureRequestBuilder setFlash(CameraFlash flash) {
            this.flash = flash;
            return this;
        }

        CaptureRequestBuilder setAutoWhiteBalance(boolean autoWhiteBalance) {
            this.autoWhiteBalance = autoWhiteBalance;
            return this;
        }

        CaptureRequestBuilder setDisplayOrientation(CameraFacing cameraFacing, int displayOrientation) {
            this.cameraFacing = cameraFacing;
            this.displayOrientation = displayOrientation;
            return this;
        }

        <T> CaptureRequestBuilder setKeyValue(@NonNull CaptureRequest.Key<T> key, T value) {
            previewRequestBuilder.set(key, value);
            return this;
        }

        public CaptureRequest build() {
            setup3AControls(previewRequestBuilder, autoFocus, flash, autoWhiteBalance);
            setupJpegOrientation(previewRequestBuilder, cameraFacing, displayOrientation);
            for (Surface surface : outputSurfaces) {
                previewRequestBuilder.addTarget(surface);
            }
            return previewRequestBuilder.build();
        }
    }
}
